FROM alpine:latest AS builder

ENV HELM_VERSION=3.9.3
ENV RELEASE_ROOT="https://get.helm.sh"
ENV RELEASE_FILE="helm-v${HELM_VERSION}-linux-amd64.tar.gz"
ENV USER=scratchuser
ENV UID=10001

RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/home/%{USER}" \    
    --shell "/sbin/nologin" \    
    --uid "${UID}" \    
    "${USER}"

RUN apk update && apk add --no-cache git ca-certificates curl git && update-ca-certificates && \
    curl -L ${RELEASE_ROOT}/${RELEASE_FILE} | tar xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm

COPY --from=bitnami/kubectl:latest /opt/bitnami/kubectl/bin/kubectl /usr/bin/kubectl

USER "${USER}"
