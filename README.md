# https://gitlab.com/dev65asia/kubectl-helm

```
REPOSITORY                                   TAG       IMAGE ID       CREATED              SIZE
registry.gitlab.com/dev65asia/kubectl-helm   latest    9461d6a7f6cc   About a minute ago   114MB
```

# getting started

run `kubectl`
```
docker run registry.gitlab.com/dev65asia/kubectl-helm:latest kubectl
```

run `helm`
```
docker run registry.gitlab.com/dev65asia/kubectl-helm:latest helm
```

# quick install (linux)
## prerequisites:
- linux
- docker

copy files locally
```
docker create -ti --name kubectl-helm registry.gitlab.com/dev65asia/kubectl-helm:latest bash && \
sudo docker cp kubectl-helm:/usr/bin/kubectl /usr/bin/kubectl && \
sudo docker cp kubectl-helm:/usr/bin/helm /usr/bin/helm && \
docker rm -f kubectl-helm
```

verify
```
which /usr/bin/kubectl
which /usr/bin/helm3
```

uninstall
```
sudo rm /usr/bin/kubectl
sudo rm /usr/bin/helm3
```

# gitlab ci
## prerequisites:
- gitlab agent
- gitlab kas

```
kubectl:
  image: 
    name: registry.gitlab.com/dev65asia/kubectl-helm:latest
    entrypoint: ['']
  script:
    - kubectl config get-contexts
    - kubectl config use-context <repo_path>:<agent_name>
    # Basic Commands
    - kubectl get pods -A
    - helm list --all-namespaces
  when: manual
```
